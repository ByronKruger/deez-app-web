export interface Album {
    id: string,
    title: string,
    year: string,
    cover: string,
}