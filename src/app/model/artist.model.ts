export interface Artist {
    name: string,
    picture: string,
    fanCount: number,
    albumCount: number
}