import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavBarComponent } from './features/nav-bar/nav-bar.component';
import { ArtistListComponent } from './features/artist-list/artist-list.component';
import { ArtistComponent } from './features/artist/artist.component';
import { ArtistDetailComponent } from './features/artist-detail/artist-detail.component';
import { ArtistSearchComponent } from './features/artist-search/artist-search.component';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { ArtistDetailCollectionComponent } from './features/artist-detail-collection/artist-detail-collection.component';

@NgModule({
  declarations: [
    AppComponent,
    NavBarComponent,
    ArtistListComponent,
    ArtistComponent,
    ArtistDetailComponent,
    ArtistSearchComponent,
    ArtistDetailCollectionComponent,
  ],
  imports: [
    FormsModule,
    BrowserModule,
    HttpClientModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
