import { of } from 'rxjs';
import { DeezerRepositoryService } from './deezer-repository.service';

import { ArtistSearchService } from './artist-search.service';
import { Artist } from '../model/artist.model';

describe('RawDataService', () => {
  let service: ArtistSearchService;
  let repository: DeezerRepositoryService;
  const exampleRawData = {
    "data": [
      {
        "id": "12246",
        "name": "Taylor Swift",
        "link": "https://www.deezer.com/artist/12246",
        "picture": "https://api.deezer.com/artist/12246/image",
        "picture_small": "https://cdns-images.dzcdn.net/images/artist/90f7f6ce342a684aca306fec31a40f69/56x56-000000-80-0-0.jpg",
        "picture_medium": "https://cdns-images.dzcdn.net/images/artist/90f7f6ce342a684aca306fec31a40f69/250x250-000000-80-0-0.jpg",
        "picture_big": "https://cdns-images.dzcdn.net/images/artist/90f7f6ce342a684aca306fec31a40f69/500x500-000000-80-0-0.jpg",
        "picture_xl": "https://cdns-images.dzcdn.net/images/artist/90f7f6ce342a684aca306fec31a40f69/1000x1000-000000-80-0-0.jpg",
        "nb_album": 55,
        "nb_fan": 8592310,
        "radio": true,
        "tracklist": "https://api.deezer.com/artist/12246/top?limit=50",
        "type": "artist"
      },
      {
        "id": "131036472",
        "name": "Taylor Brandton",
        "link": "https://www.deezer.com/artist/131036472",
        "picture": "https://api.deezer.com/artist/131036472/image",
        "picture_small": "https://cdns-images.dzcdn.net/images/artist/fd25f5d256199ddd60912628121e0d83/56x56-000000-80-0-0.jpg",
        "picture_medium": "https://cdns-images.dzcdn.net/images/artist/fd25f5d256199ddd60912628121e0d83/250x250-000000-80-0-0.jpg",
        "picture_big": "https://cdns-images.dzcdn.net/images/artist/fd25f5d256199ddd60912628121e0d83/500x500-000000-80-0-0.jpg",
        "picture_xl": "https://cdns-images.dzcdn.net/images/artist/fd25f5d256199ddd60912628121e0d83/1000x1000-000000-80-0-0.jpg",
        "nb_album": 1,
        "nb_fan": 75,
        "radio": true,
        "tracklist": "https://api.deezer.com/artist/131036472/top?limit=50",
        "type": "artist"
      },
      {
        "id": "4680970",
        "name": "Natalie Taylor",
        "link": "https://www.deezer.com/artist/4680970",
        "picture": "https://api.deezer.com/artist/4680970/image",
        "picture_small": "https://cdns-images.dzcdn.net/images/artist/0ce367354ab46b3bb5ec82868dda2719/56x56-000000-80-0-0.jpg",
        "picture_medium": "https://cdns-images.dzcdn.net/images/artist/0ce367354ab46b3bb5ec82868dda2719/250x250-000000-80-0-0.jpg",
        "picture_big": "https://cdns-images.dzcdn.net/images/artist/0ce367354ab46b3bb5ec82868dda2719/500x500-000000-80-0-0.jpg",
        "picture_xl": "https://cdns-images.dzcdn.net/images/artist/0ce367354ab46b3bb5ec82868dda2719/1000x1000-000000-80-0-0.jpg",
        "nb_album": 22,
        "nb_fan": 15660,
        "radio": true,
        "tracklist": "https://api.deezer.com/artist/4680970/top?limit=50",
        "type": "artist"
      }
    ],
    "total": 300,
    "next": "https://api.deezer.com/search/artist?q=taylor&index=25"
  }
  let artistsSubjectSubscribe: Artist[];

  beforeEach(() => {
    repository = jasmine.createSpyObj('DeezerRepositoryService', ['getArtistRaw']);
    service = new ArtistSearchService(repository);
    service.artistsSubject.subscribe(artists => {
      artistsSubjectSubscribe = artists;
    });
  });

  // Removed due to refactoring 
  // 
  // it('should return artist list', (done: DoneFn) => {
  //   (repository as any).getArtistRaw.and.returnValue(of(exampleRawData));

  //   service.getArtist("taylor_swift");
  //   expect(service.artists).toBeTruthy();
  //   expect(service.artists?.length).toBe(3);  
  //   done();
  // })

  // it('should populate artist list', (done: DoneFn) => {
  //   (repository as any).getArtistRaw.and.returnValue(of(exampleRawData));
  //   service.getArtist("taylor swift");
  //   expect(artistsSubjectSubscribe.length).toBe(3);
  //   artistsSubjectSubscribe.forEach((artist: Artist) => {
  //     expect(artist.name).toBeTruthy();
  //     expect(artist.picture).toBeTruthy();
  //     expect(artist.fanCount).toBeTruthy();
  //     expect(artist.albumCount).toBeTruthy();     
  //   })
  //   done();
  // })

  it("should have Artist[] property and getter", () => {
    expect(service.artists).toBeUndefined();
  })

});
