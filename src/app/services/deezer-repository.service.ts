import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { concatAll, concatMap, map, mergeMap, toArray } from 'rxjs/operators';
import { Album } from '../model/album.model';
import { Artist } from '../model/artist.model';
import { Track } from '../model/track.model';
import { IRepository } from './IRepository';

@Injectable({
  providedIn: 'root'
})
export class DeezerRepositoryService implements IRepository {
  baseUrl = "http://localhost:4200/api/";

  constructor(private http: HttpClient){}

  getArtist(name: string): Observable<Artist[]> {
    return this.http.get(this.baseUrl+"search/artist?q="+name).pipe(
      map((artists: any) => {
        return artists.data.map((artist: any)=>{
          return {
            id: artist.id,
            picture: artist.picture_big,
            fanCount: artist.nb_fan,
            albumCount: artist.nb_album,
            name: artist.name,
          }
        })
      })
    );
  }

  getArtistAlbums(name: string): Observable<Album[]> {
    return this.http.get(this.baseUrl+"search/album?q="+name).pipe(
      map((albums: any) => {
        return albums.data.map((album: any) => {
          return {
            id: album.id,
          }
        })
      })
    );
  }

  getArtistAlbums1(urlObs: Observable<string>): Observable<Object> {
    return urlObs.pipe(
      map(url => this.http.get(this.baseUrl + url)),
      concatAll(),
      toArray()
    );
  }

  // getArtistAlbums2(albumsPartial: Album[]): Observable<Album[]> {
  //   return this.http.get(this.baseUrl+"album/"+albumsPartial.i).pipe(
  //     map((albumsRaw: any) => {
  //       return albumsRaw.data;
  //     }),
  //     map((albumRawArray: any) => {
  //       return albumRawArray.map((albumRaw: any) => {
  //         return {
  //           id: albumRaw.id,
  //           title: albumRaw.title,
  //           cover: albumRaw.cover_xl,
  //         }
  //       })
  //     })
  //     // map((albums: Album[]) => {
  //     //   return albums.map((album: Album) => {
  //     //     return this.http.get(this.baseUrl + "album/"+album.id)
  //     //   }),
  //     //   concatAll()
  //     // })
  //     // map((albumsRawArray: any) => {
  //       // return {
  //       //   id: albumsRawArray.id,
  //       //   title: "string",
  //       //   year: "string",
  //       //   cover: "string",
  //       // }
  //     // }),
  //   )
  // }

  getAlbumsYears(albumObservables: Observable<Album[]>): Observable<Album[]> {
    // return this.http.get(this.baseUrl+"search/album?q="+name).pipe(
    //   map((albumsRaw: any) => {
    //     return albumsRaw.data.map((album: any) => {
    //       return {
    //         id: album.id,
    //       }
    //     })
    //   }),
    return albumObservables.pipe(
      map((albums: any) => {
        return albums.map((album: any) => {
          return this.http.get(this.baseUrl+"album/"+album.id).pipe(
            map((albumRaw: any) => {
              return {
                id: albumRaw.id,
                title: albumRaw.title,
                year: albumRaw.release_date,
                cover: albumRaw.cover_xl   
              }
            })
          )
        })
      })
    )
    
      // mergeMap(
      //   (albumsRaw: any) => {
      //     return { 
      //       id:
      //     }
      //   }, 
      //   () => {

      //   }, 1)

      // mergeMap((albums: any) => {
      //   return albums.data.map((album: any) => {
      //     return {
      //       id: album.id
      //     }
      //   })
      // })
    // )
  }
  
  getArtistTracks(name: string): Observable<Track[]> {
      return this.http.get(this.baseUrl+"search/track?q="+name).pipe(
      map((tracks: any) => {
        return tracks.data.map((track: any) => {
          return {
            title: track.title,
            duration: track.duration,
            rank: track.rank,
          }
        })
      })
    );
  }

  getArtistById(id: string): Observable<Artist[]> {
    return this.http.get(this.baseUrl+"search/artist?q="+id).pipe(
      // return this.http.get(this.baseUrl+"artist/"+id).pipe(
      map((artists: any) => {
        return artists.data.map((artist: any) => {
          return {
            id: artist.id,
            picture: artist.picture_big,
            fanCount: artist.nb_fan,
            name: artist.name,
            albumCount: artist.nb_album
          }
        })
      })
    )
  } 
}
