import { TestBed } from '@angular/core/testing';
import { DeezerRepositoryService } from './deezer-repository.service';

import { GetTracksService } from './get-tracks.service';
import { Track } from '../model/track.model';
import { of, Subject } from 'rxjs';

describe('GetTracksService', () => {
  let service: GetTracksService;
  let repository: DeezerRepositoryService;
  let subject: Subject<Track[]>;
  const tracksData = {
    "data": [
      {
        "id": "6461440",
        "readable": true,
        "title": "Love The Way You Lie",
        "title_short": "Love The Way You Lie",
        "title_version": "",
        "link": "https://www.deezer.com/track/6461440",
        "duration": "263",
        "rank": "923450",
        "explicit_lyrics": true,
        "explicit_content_lyrics": 1,
        "explicit_content_cover": 1,
        "preview": "https://cdns-preview-1.dzcdn.net/stream/c-1ed50e5b3118c99be858fc305609e62a-15.mp3",
        "md5_image": "be682506145061814eddee648edb7c59",
        "artist": {
          "id": "13",
          "name": "Eminem",
          "link": "https://www.deezer.com/artist/13",
          "picture": "https://api.deezer.com/artist/13/image",
          "picture_small": "https://cdns-images.dzcdn.net/images/artist/0707267475580b1b82f4da20a1b295c6/56x56-000000-80-0-0.jpg",
          "picture_medium": "https://cdns-images.dzcdn.net/images/artist/0707267475580b1b82f4da20a1b295c6/250x250-000000-80-0-0.jpg",
          "picture_big": "https://cdns-images.dzcdn.net/images/artist/0707267475580b1b82f4da20a1b295c6/500x500-000000-80-0-0.jpg",
          "picture_xl": "https://cdns-images.dzcdn.net/images/artist/0707267475580b1b82f4da20a1b295c6/1000x1000-000000-80-0-0.jpg",
          "tracklist": "https://api.deezer.com/artist/13/top?limit=50",
          "type": "artist"
        },
        "album": {
          "id": "595243",
          "title": "Recovery",
          "cover": "https://api.deezer.com/album/595243/image",
          "cover_small": "https://cdns-images.dzcdn.net/images/cover/be682506145061814eddee648edb7c59/56x56-000000-80-0-0.jpg",
          "cover_medium": "https://cdns-images.dzcdn.net/images/cover/be682506145061814eddee648edb7c59/250x250-000000-80-0-0.jpg",
          "cover_big": "https://cdns-images.dzcdn.net/images/cover/be682506145061814eddee648edb7c59/500x500-000000-80-0-0.jpg",
          "cover_xl": "https://cdns-images.dzcdn.net/images/cover/be682506145061814eddee648edb7c59/1000x1000-000000-80-0-0.jpg",
          "md5_image": "be682506145061814eddee648edb7c59",
          "tracklist": "https://api.deezer.com/album/595243/tracks",
          "type": "album"
        },
        "type": "track"
      },
      {
        "id": "1579906",
        "readable": true,
        "title": "8 Mile (Soundtrack Version Edit)",
        "title_short": "8 Mile",
        "title_version": "(Soundtrack Version Edit)",
        "link": "https://www.deezer.com/track/1579906",
        "duration": "359",
        "rank": "684189",
        "explicit_lyrics": false,
        "explicit_content_lyrics": 3,
        "explicit_content_cover": 2,
        "preview": "https://cdns-preview-9.dzcdn.net/stream/c-917cd35c728bd3550437db7788fb4b54-7.mp3",
        "md5_image": "6fa1402225904c274083a73f6618cdcf",
        "artist": {
          "id": "13",
          "name": "Eminem",
          "link": "https://www.deezer.com/artist/13",
          "picture": "https://api.deezer.com/artist/13/image",
          "picture_small": "https://cdns-images.dzcdn.net/images/artist/0707267475580b1b82f4da20a1b295c6/56x56-000000-80-0-0.jpg",
          "picture_medium": "https://cdns-images.dzcdn.net/images/artist/0707267475580b1b82f4da20a1b295c6/250x250-000000-80-0-0.jpg",
          "picture_big": "https://cdns-images.dzcdn.net/images/artist/0707267475580b1b82f4da20a1b295c6/500x500-000000-80-0-0.jpg",
          "picture_xl": "https://cdns-images.dzcdn.net/images/artist/0707267475580b1b82f4da20a1b295c6/1000x1000-000000-80-0-0.jpg",
          "tracklist": "https://api.deezer.com/artist/13/top?limit=50",
          "type": "artist"
        },
        "album": {
          "id": "162035",
          "title": "8 Mile",
          "cover": "https://api.deezer.com/album/162035/image",
          "cover_small": "https://cdns-images.dzcdn.net/images/cover/6fa1402225904c274083a73f6618cdcf/56x56-000000-80-0-0.jpg",
          "cover_medium": "https://cdns-images.dzcdn.net/images/cover/6fa1402225904c274083a73f6618cdcf/250x250-000000-80-0-0.jpg",
          "cover_big": "https://cdns-images.dzcdn.net/images/cover/6fa1402225904c274083a73f6618cdcf/500x500-000000-80-0-0.jpg",
          "cover_xl": "https://cdns-images.dzcdn.net/images/cover/6fa1402225904c274083a73f6618cdcf/1000x1000-000000-80-0-0.jpg",
          "md5_image": "6fa1402225904c274083a73f6618cdcf",
          "tracklist": "https://api.deezer.com/album/162035/tracks",
          "type": "album"
        },
        "type": "track"
      }
    ],
    "total": 300,
    "next": "https://api.deezer.com/search/track?q=eminem&index=25"
  }

  beforeEach(() => {
    repository = jasmine.createSpyObj('DeezerRepositoryService', ['getArtistTracks']);
    subject = jasmine.createSpyObj('tracksSubject', ['next']);
    service = new GetTracksService(repository);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should get tracks', () => {
    (repository as any).getArtistTracks.and.returnValue(of(tracksData));
    service.getArtistTracks("Taylor Swift");
    service.tracksSubject.subscribe((tracks: Track[]) => {
      service.getArtistTracks("Taylor Swift");
      expect(tracks.length).toBe(2);
      expect(subject.next).toHaveBeenCalled()
      expect(repository.getArtistAlbums).toHaveBeenCalled()
    });
  })
});
