import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ArtistDetailCollectionComponent } from './features/artist-detail-collection/artist-detail-collection.component';
import { ArtistListComponent } from './features/artist-list/artist-list.component';
import { ArtistComponent } from './features/artist/artist.component';

const routes: Routes = [
  { path: 'artists', component: ArtistComponent },
  { path: 'artist-detail/:name', component: ArtistDetailCollectionComponent },
  { path: '', redirectTo: 'artists', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
