import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Artist } from 'src/app/model/artist.model';
import { ArtistSearchService } from '../../services/artist-search.service';

@Component({
  selector: 'app-artist-list',
  templateUrl: './artist-list.component.html',
  styleUrls: ['./artist-list.component.scss']
})
export class ArtistListComponent implements OnInit{
  artists!: Artist[]

  constructor(private artistService: ArtistSearchService){}

  ngOnInit(): void {
    this.artistService.artistsSubject.subscribe((artists: Artist[]) => {
      this.artists = artists;
    })
  }
}