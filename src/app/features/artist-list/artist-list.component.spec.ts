import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ArtistSearchService } from 'src/app/services/artist-search.service';
import { ArtistListComponent } from './artist-list.component';

describe('ArtistListComponent', () => {
  let component: ArtistListComponent;
  let fixture: ComponentFixture<ArtistListComponent>;
  let artistDataService: ArtistSearchService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ArtistListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ArtistListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    artistDataService = jasmine.createSpyObj('artistDataService', ['getArtist']);
  });

  // it('should list artists', () => {
  //   const exampleArtists = [{picture: 'https://cdns-images.dzcdn.net/images/artist/90f7f6ce342a684aca306fec31a40f69/500x500-000000-80-0-0.jpg', fanCount: 8592310, albumCount: 55}, {picture: 'https://cdns-images.dzcdn.net/images/artist/fd25f5d256199ddd60912628121e0d83/500x500-000000-80-0-0.jpg', fanCount: 75, albumCount: 1}, {picture: 'https://cdns-images.dzcdn.net/images/artist/0ce367354ab46b3bb5ec82868dda2719/500x500-000000-80-0-0.jpg', fanCount: 15660, albumCount: 22}];
  //   (artistDataService as any).getArtist.and.returnValue(of(exampleArtists));
  //   const artistListElement: HTMLElement = fixture.nativeElement;
  //   expect(artistListElement.textContent).toContain("https://cdns-images.dzcdn.net/images/artist/90f7f6ce342a684aca306fec31a40f69/500x500-000000-80-0-0.jpg");
  // })

});
