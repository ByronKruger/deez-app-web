import { Component, Input, OnInit } from '@angular/core';
import { Album } from 'src/app/model/album.model';
import { Artist } from 'src/app/model/artist.model';
import { Track } from 'src/app/model/track.model';

@Component({
  selector: 'app-artist-detail',
  templateUrl: './artist-detail.component.html',
  styleUrls: ['./artist-detail.component.scss']
})
export class ArtistDetailComponent {
  @Input() artist!: Artist;
  @Input() albums!: Album[];
  @Input() tracks!: Track[];
}
