import { Component, OnInit } from '@angular/core';
import { ArtistSearchService } from 'src/app/services/artist-search.service';

@Component({
  selector: 'app-artist-search',
  templateUrl: './artist-search.component.html',
  styleUrls: ['./artist-search.component.scss']
})
export class ArtistSearchComponent {
  public name!: string;

  constructor(private searchService: ArtistSearchService) {}

  public onSubmit(): void {
    this.searchService.getArtist(this.name);
  }
}
