import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ArtistDetailCollectionComponent } from './artist-detail-collection.component';

describe('ArtistDetailCollectionComponent', () => {
  let component: ArtistDetailCollectionComponent;
  let fixture: ComponentFixture<ArtistDetailCollectionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ArtistDetailCollectionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ArtistDetailCollectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  // it('should create', () => {
  //   expect(component).toBeTruthy();
  // });
});
