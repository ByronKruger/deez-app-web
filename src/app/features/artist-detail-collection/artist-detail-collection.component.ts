import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { map } from 'rxjs/operators';
import { Album } from 'src/app/model/album.model';
import { Artist } from 'src/app/model/artist.model';
import { Track } from 'src/app/model/track.model';
import { ArtistSearchService } from 'src/app/services/artist-search.service';
import { GetAlbumService } from 'src/app/services/get-album.service';
import { GetTracksService } from 'src/app/services/get-tracks.service';

@Component({
  selector: 'app-artist-detail-collection',
  templateUrl: './artist-detail-collection.component.html',
  styleUrls: ['./artist-detail-collection.component.scss']
})
export class ArtistDetailCollectionComponent implements OnInit{
  albums!: Album[];
  tracks!: Track[];
  artist!: Artist;
  name!: string;
  id!: number;

  constructor(private route: ActivatedRoute,
              private albumService: GetAlbumService,
              private trackService: GetTracksService,
              private artistSearchService: ArtistSearchService,
              ) { }

  public ngOnInit(): void {
    this.artistSearchService.artistSubject.subscribe((artist: Artist) => {
      this.artist = artist;
    })

    this.albumService.albumsSubject.subscribe((albums: Album[]) => {
      this.albums = albums;
    });

    this.trackService.tracksSubject.subscribe((tracks: Track[]) => {
      this.tracks = tracks;
    });

    this.route.paramMap.subscribe((params: ParamMap) => {
      this.name = params.get("name")!;
      this.artistSearchService.getArtistById(this.name);
      this.trackService.getArtistTracks(this.name);
      this.albumService.getArtistAlbums(this.name);
    })
  }
}